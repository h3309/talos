import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/IndexPage.vue') }],
  },
  {
    path: '/partner',
    component: () => import('layouts/PartnerLayout.vue'),
    children: [{ path: '', component: () => import('pages/PartnerLogin.vue') }],
  },
  {
    path: '/filedrop',
    component: () => import('layouts/PartnerLayout.vue'),
    children: [{ path: '', component: () => import('pages/PartnerFileDrop.vue') }],
  },
  {
    path: '/analyst',
    component: () => import('layouts/InventoryLayout.vue'),
    children: [{ path: '', component: () => import('pages/DataAnalyst.vue') }],
  },
  {
    path: '/inventory',
    component: () => import('layouts/InventoryLayout.vue'),
    children: [{ path: '', component: () => import('pages/InventoryDashboard.vue') }],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
