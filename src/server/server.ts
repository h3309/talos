const crud = require('./crud');

// crud.validateBizPartner('a', 'b').then((u:boolean) => console.log('So we gota a', u));

const fs = require('fs');
const file = fs.createReadStream('./src/server/data.csv');
const Papa = require('papaparse');
// const csvData: string[] = [];

// const printme = (arr: string[]) => arr.map((x)=>console.log('lol', x));
const printme = (arr: string[]) => {
  arr.map((res) => {
    const row = JSON.parse(JSON.stringify(res));
    crud.createBizPartner(row.companyName, row.username, row.password);
  })
};

const read = () => {
  const csvData: string[] = [];
  Papa.parse(file, {
    //worker: true,
    header: true,
    step: function(result:any) {
      csvData.push((result.data) as string)
    //   console.log('pushin p', result.data);
    },
    complete: function() {
      console.log('Completed', csvData.length, 'records');
      printme(csvData);
      // (() => {
      //   csvData.map((res) => {
      //     console.log('whoop whoop');
      //     const row = JSON.parse(JSON.stringify(res));
      //     crud.testFindCountUsers(row.password); // executes 5 times // should be used for find one, since record is read one by one
      //   })
      // })()
      // (() => {
      //   csvData.map((res) => {
      //     console.log('whoop whoop');
      //     const row = JSON.parse(JSON.stringify(res));
      //     crud.testFindCountUsers(row.password); // executes 5 times // should be used for find one, since record is read one by one
      //   })
      // })()
    },
    error: function() {
      console.log('Done goofed');
    }
  });
}

read();
// const thing = async() => {
//   await read().then((a) => console.log('uhh', a));
// }

// read((thing: string[]) => thing.map((res) => console.log('separated!',res)))
// read((thing: string[]) => thing.map((res) => {
//   const a = JSON.parse(JSON.stringify(res));
//   crud.createBizPartner(a.companyName, a.username, a.password);
// }))


// thing().then(() => console.log('eeyy'));

// for (let i = 0; i<csvData.length; i++) {
//   console.log(csvData[i]);
// }

// export default res;


// demoGet();
module.exports = read;
