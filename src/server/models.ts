const { Sequelize, DataTypes, Model } = require('@sequelize/core');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './src/server/db.sqlite',
});

class BizPartner extends Model {}
BizPartner.init(
  {
    companyName: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
  },
  { sequelize, modelName: 'bizpartner' }
);

class Inventory extends Model {}
Inventory.init({
  itemName:   DataTypes.STRING,
  itemCode:   DataTypes.STRING,
  category:   DataTypes.STRING,
  quantity:   DataTypes.INTEGER,
  condition:  DataTypes.STRING,
  note:       DataTypes.STRING,
}, { sequelize, modelName:'inventory'});

module.exports = { sequelize, BizPartner, Inventory };
